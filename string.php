<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h2> Contoh Soal </h2>
    <?php

    echo "<h3> Contoh 1 </h3>";
    $kalimat1 = "Hello World" ;
    echo "kalimat pertama : " . $kalimat1 . "<br>";
    echo "panjang string : " . strlen($kalimat1) . "<br>";
    echo "jumlah kata : " . str_word_count ($kalimat1) . " <br> </br> ";

    echo "<h3> Contoh 2 </h3>";
    $string2 = "nama saya riza" ;
    echo "kalimat kedua : " . $string2 . "<br>";
    echo "kata pertama : " . substr($string2, 0,4) . "<br>";
    echo "kata kedua : " . substr($string2, 5,4) . "<br>";
    echo "kata ketiga : " . substr($string2, 10,4) . "<br>";

    echo "<h3> Contoh 3 </h3>";
    $string3 = "selamat pagi";
    echo "kalimat ketiga : " . $string3. "<br>";
    echo "ganti kalimat ketiga : " . str_replace("pagi","malam",$string3) ;

    ?>
</body>
</html>